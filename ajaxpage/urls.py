from django.urls import path
from .views import story8index,panggil_buku
#url for app

app_name = "story8"

urlpatterns = [
    path('', story8index,name ='story8index'),
    path('panggil_buku/',panggil_buku),
]