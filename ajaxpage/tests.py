from django.test import TestCase,Client,LiveServerTestCase
from django.urls import resolve
from .views import story8index,panggil_buku
import json

# Create your tests here.
class Story8Test(TestCase):
    def test_url_bisa_diakses(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code,200)
    
    def test_fungsi_view(self):
        fungsi = resolve('/story8/')
        self.assertEqual(fungsi.func,story8index)
    
    def test_url_AJAX(self):
        response = self.client.get("/story8/panggil_buku/")
        self.assertEqual(response.status_code,200)

    def test_page_pakai_AJAX(self):
        response = resolve("/story8/panggil_buku/")
        self.assertEqual(response.func,panggil_buku)
    
    def test_template(self):
        response = self.client.get("/story8/")
        self.assertTemplateUsed(response,"story8index.html")

    def test_response_JSON_AJAX(self):
        response = self.client.get("/story8/panggil_buku/")
        self.assertEqual(type(json.loads(response.content)),dict)