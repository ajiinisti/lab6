from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.
def story8index(request):
    r = requests.get('https://www.googleapis.com/books/v1/volumes?q=naruto')
    return render(request,'story8index.html',{'data_buku':r.json()})

def panggil_buku(request):
    q = request.GET.get('q','Jaja')
    r = requests.get('https://www.googleapis.com/books/v1/volumes?q='+q)
    return JsonResponse(r.json())