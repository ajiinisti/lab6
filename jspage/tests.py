from django.test import TestCase,Client,LiveServerTestCase
from django.urls import resolve
from .views import index
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
# Create your tests here.

class JspageTest(TestCase):
    def test_url_bisa_diakses(self):
        response = Client().get('/jspage/')
        self.assertEqual(response.status_code,200)
    
    def test_fungsi_view(self):
        fungsi = resolve('/jspage/')
        self.assertEqual(fungsi.func,index)

    def test_template(self):
        response = self.client.get('/jspage/')
        self.assertTemplateUsed(response, 'index.html')

    def test_isi(self):
        response = Client().get('/jspage/')
        isi_html = response.content.decode("utf8")
        self.assertIn("Aktivitas",isi_html)
        self.assertIn("Kepanitiaan",isi_html)
        self.assertIn("Prestasi",isi_html)

