from django.db import models
from django.utils import timezone
# Create your models here.
class statusModel(models.Model):
    nama = models.CharField(max_length = 20)
    status = models.CharField(max_length = 300)
    waktu = models.DateTimeField(default = timezone.now)
