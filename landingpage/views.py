from django.shortcuts import render,redirect
from .forms import statusForm
from .models import statusModel
# Create your views here.

def index(request):
    form = statusForm(request.POST or None)
    data = statusModel.objects.all().order_by('-id')
    isian = {
            'generated_formulir': form,
            'generated_model' : data
        }
    if request.method == 'POST':
        model = statusModel(nama = form['nama'].value(),status = form['status'].value())
        model.save()
        return redirect("landingpage:index")
    return render(request,'landingpage/index.html', isian)
    