from django.test import TestCase,Client,LiveServerTestCase
from django.urls import resolve
from .models import statusModel
from .forms import statusForm
from .views import index
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.

class landingPageTest(TestCase):
    def test_url_bisa_diakses(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def test_fungsi_view(self):
        fungsi = resolve('/')
        self.assertEqual(fungsi.func,index)

    def test_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'landingpage/index.html')

    def test_isi(self):
        response = Client().get('')
        isi_html = response.content.decode("utf8")
        self.assertIn("Halo, apa kabar?",isi_html)
        self.assertIn("<form",isi_html)
    
    def test_view_post(self):
        data ={ 'nama' : 'aji', 'status':'Haii!!'}
        response = Client().post('',data)
        data = statusModel.objects.all().count()
        self.assertEqual(data,1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'],'/')
        new_response = self.client.get('')
        html_response = new_response.content.decode("utf8")
        self.assertIn("Haii!!",html_response)

class functionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    
    def tearDown(self):
        self.browser.quit()

    def test_masukkan_data(self):
        self.browser.get(self.live_server_url)
        nama = self.browser.find_element_by_id('id_nama')
        nama.send_keys('Aji')
        status = self.browser.find_element_by_id('id_status')
        status.send_keys("Ingin bermain")
        status.submit()
        self.assertIn("Aji",self.browser.page_source)
        self.assertIn("Ingin bermain",self.browser.page_source)
        time.sleep(5)
