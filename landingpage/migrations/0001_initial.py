# Generated by Django 2.2.5 on 2019-10-31 16:45

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='statusModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=20)),
                ('status', models.CharField(max_length=300)),
                ('waktu', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
    ]
